#!/usr/bin/python
#
# Copyright (c) 2019, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# *  Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# *  Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# *  Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import xml.etree.ElementTree as ET
import re
import sys

if len(sys.argv) != 2:
    print  # Empty line
    print sys.argv[0], "help:\n"
    print "Please pass the rd1 file to this script, e.g.:"
    print "", sys.argv[0], "am335x-padconf_yyyy-mm-dd_hhmmss.rd1\n"
    print("Output file will have same base name as input, "
          "but csv file type.\n")
    sys.exit(1)

try:
    rd1 = open(sys.argv[1], "rt")
except IOError:
    print "Error: input file", sys.argv[1], "not accessible."
    sys.exit(1)

try:
    csv_filename = sys.argv[1]
    csv_filename = csv_filename.replace(".rd1", ".csv")
    csv = open(csv_filename, "w+")
except IOError:
    print "Error creating file", csv_filename

# CSV files must use \r\n for all line endings
# Create header row
csv.write("Reg Name,Reg Address,Reg Value,Slew,InputEnable,Pull Config,"
          "Muxmode,Mux Description,Comments\r\n")
csv_string = "%s,0x%08X,0x%08X,%s,%s,%s,%d,%s,%s\r\n"

# Determine device family based on header
header = rd1.readline()
m = re.match(r'.*AM335x.*', header, 0);
if m is None:
    print "Unrecognized device family in header:"
    print header
    sys.exit(1)

# Open pinmux data file
try:
    data_file = open("am335x-pinmux.data", "rt")
except IOError:
    print "Error: Could not open am335x-pinmux.data."
    sys.exit(1)

# Read header row from pinmux data file (throw away)
data_file.readline()

for lines in rd1:
    # Use regular expression to extract address and data from rd1
    m = re.match(r'(0x[0-9a-fA-F]{8})\s+(0x[0-9a-fA-F]{8})', lines, 0)
    if m:
        alphanum1 = m.group(1)  # address (string)
        alphanum2 = m.group(2)  # data (string)
        address = int(alphanum1, 16)  # convert from string to number
        offset = address - 0x44E10000
        register_value = int(alphanum2, 16)  # convert from string
        comments = ""  # Start new for each line

        # Extract slewcontrol field
        if (register_value & 0x00000040) == 0x00000040:
            slewstring = "slow"
            comments += "Slow slew not allowed"
        else:
            slewstring = "fast"

        # Extract inputenable and pullup/pulldown info
        if (register_value & 0x00000020) == 0x00000020:
            inputenable_string = "input enabled"
        else:
            inputenable_string = "output-only"
        if (register_value & 0x00000008) == 0x00000008:
            pullstring = "no pull"
        else:
            if (register_value & 0x00000010) == 0x00000010:
                pullstring = "pullup"
            else:
                pullstring = "pulldown"

        # Extract muxmode and associated description
        muxmode = register_value & 0x7
	mux_data = data_file.readline()
        m = re.match(r'(.*),(0x[0-9a-fA-F]{8}),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(\S*)', mux_data, 0)
        if m:
            register_name = m.group(1)
            data_file_address = int(m.group(2), 16)
            if data_file_address != address:
                print "Address mismatch error. Address %08X from rd1 doesn't match %08X from data file." % (address, data_file_address)
            muxmode_description = m.group(muxmode + 3)
        else:
            print "Could not find matching mux mode for address %08X muxmode %d" % (address, muxmode)

        # Write a line of the CSV file
        csv.write(csv_string % (register_name, address,
                  register_value, slewstring, inputenable_string,
                  pullstring, muxmode, muxmode_description, comments))

rd1.close()
csv.close()

